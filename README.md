# Flask internet speed test

Using Flask test internet speed 




## What does internet speed test mean?

_Speedtest measures the speed between 
your device and a test server, using your device's internet connection. This means you might get one Speedtest result on one device and a different result on another, even using the same provider. ...
Some devices may not be able to measure the
full speed of your internet service._

### pip / easy_install

> pip install speedtest-cli

### how this web app work.?
#### Python API
1. Speedtest.net has migrated to using pure socket tests instead of HTTP based tests
2. This application is written in Python
3. Different versions of Python will execute certain parts of the code faster than others
4. CPU and Memory capacity and speed will play a large part in inconsistency between Speedtest.net and even other machines on the same network

### Local host 
![alt text](https://xp.io/storage/1BIF2Ci7.png)

### Click Post method 
![alt text](https://xp.io/storage/1BIM6ro6.png)

### successfull post method

![alt text](https://xp.io/storage/1BIR1zD2.png)

### Summary :

##### PING : _5Ms_
##### Download Speed : _4Mb_
##### Upload Speed : _8Mb_
