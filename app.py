from flask import Flask ,render_template,request
import speedtest
import time
import math

app = Flask(__name__)

@app.route('/')
@app.route('/home')
def home():
    return render_template("home.html")

@app.route('/send',methods=['POST'])
def send():
    if request.method == "POST":
        mb = 0.000001
        s = speedtest.Speedtest()
        u = (s.upload()*mb)
        upload = math.floor(u)
        d = (s.download()*mb)
        download = math.floor(d)
        server = []
        s.get_servers(server)
        p = (s.results.ping)
        ping = math.floor(p)
        return render_template('home.html',download=download,upload=upload,ping=ping)
    else:
        return render_template('home.html',download=0,upload=0,ping=0)

if __name__ == "__main__":
    app.run(debug=True)


